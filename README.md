# eslint-plugin-no-hardcode

ESLint plugin for warn with hardcoded literal variables

## Installation

You'll first need to install [ESLint](https://eslint.org/):

```sh
npm i eslint --save-dev
```

Next, install `eslint-plugin-no-hardcode`:

```sh
npm install eslint-plugin-no-hardcode --save-dev
```

## Usage

Add `1` to the plugins section of your `.eslintrc` configuration file. You can omit the `eslint-plugin-` prefix:

```json
{
    "plugins": [
        "no-hardcode"
    ]
}
```


Then configure the rules you want to use under the rules section.

```json
{
    "rules": {
       "no-hardcode/no-hardcoded-strings": "warn"
    }
}
```

